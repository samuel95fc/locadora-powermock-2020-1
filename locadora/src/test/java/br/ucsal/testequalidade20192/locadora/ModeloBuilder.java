package br.ucsal.testequalidade20191.locadora;

import br.ucsal.testequalidade20191.locadora.dominio.Modelo;

public class ModeloBuilder {

	private static final Integer seqCB = 0;

	private static final Integer codigoCB = 1;

	private static final String nomeCB = "OP";

	private Integer seq = seqCB;

	private Integer codigo = codigoCB;

	private String nome = nomeCB;

	public ModeloBuilder() {

	}

	public static ModeloBuilder insertModelo() {
		return new ModeloBuilder();

	}

	public ModeloBuilder insertNome(String nome) {
		this.nome = nome;
		return this;

	}

	public ModeloBuilder insertSeq(Integer seq) {
		this.seq = seq;
		return this;

	}

	public ModeloBuilder insertCodigo(Integer codigo) {
		this.codigo = codigo;
		return this;

	}

	public ModeloBuilder insert() {
		return insertModelo().insertNome(nome).insertCodigo(codigo).insertSeq(seq);
	}

	public Modelo Builder() {
		Modelo modelo = new Modelo(nome);
		modelo.setNome(nome);
		return modelo;
	}

}
