package br.ucsal.testequalidade20192.locadora;

import br.ucsal.testequalidade20191.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20191.locadora.dominio.Cliente;
import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20191.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20191.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20191.locadora.persistence.VeiculoDAO;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um veiculo disponivel para um cliente cadastrado, um
	 * contrato de locacao ha inserido.
	 * 
	 * Metodo:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observacao1: lembre-se de mocar os metodos necessarios nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observacao2: lembre-se de que o metodo locarVeiculos ha um metodo command.
	 * 
	 * @throws Exception
	 */
	
	@Mock
	private ClienteDAO clienteDAOMock;
	
	@Mock
	private VeiculoDAO veiculoDAOMock;
	
	@Mock
	private LocacaoDAO locaacaoMock;
	
	@InjectMocks
	private LocaacoBO locacaoBO;
	
	
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		
		String cpf = "12345678909";
		Cliente cliente = ClienteBuilder.insertCliente().insertCpf(cpf).builder();
		Mockito.doReturn(cliente).when(clienteDAOMok).obterPorCpf(cpf);
		SituacaoVeiculoEnum situacao = SituacaoVeiculoEnum.DISPONIVEL;
		
		String placa = "OOO1234";
		Veiculo veiculo = VeiculoBuilder.insertVeiculo().insertPlaca(placa).insertSituacaoDisponivel(situacao).Builder();
		Mockito.doReturn(veiculo).when(veiculoDAOMok).obterPorPlaca(placa);

		List<String> placas = Arrays.asList("KKK4321");
		Date dataLocacao = null;
		Integer quantidadeDiasLocacao = null;

		Mockito.verify(clienteDAOMok).obterPorCpf(cpf);
		Mockito.verify(veiculoDAOMok).obterPorPlaca(placa);

	}
}
