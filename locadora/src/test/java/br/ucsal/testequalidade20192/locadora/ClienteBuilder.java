package br.ucsal.testequalidade20192.locadora;

import br.ucsal.testequalidade20191.locadora.dominio.Cliente;

public class ClienteBuilder {
	
	private static final String cpfCB = "09876543212";
	
	private static final String nomeCB = "Samoel";
	
	private static final String telefoneCB = "7199887766";
	
	private String cpf = cpfCB;
	private String nome = nomeCB;
	private String telefone = telefoneCB;
	
	public ClienteBuilder() {
		
	}
	
	public static ClienteBuilder insertCliente() {
		return new ClienteBuilder();
	}
	
	public ClienteBuilder insertCPF(String cpf) {
		this.cpf = cpf;
		return this;
	}
	
	public ClienteBuilder insertNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public ClienteBuilder insertTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}
	
	public ClienteBuilder insert() {
		return insertCliente().insertCPF(cpf).insertNome(nome).insertTelefone(telefone);
	}
	
	public Cliente builder() {
		Cliente cliente = new Cliente(cpf, nome, telefone);
		cliente.setcpf(cpf);
		cliente.setNome(nome);
		cliente.setTelefone(telefone);
		return cliente;
	}
}
