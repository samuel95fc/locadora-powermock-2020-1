package br.ucsal.testequalidade20191.locadora;

import br.ucsal.testequalidade20191.locadora.dominio.Modelo;
import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.dominio.enums.SituacaoVeiculoEnum;


public class VeiculoBuilder {
	
	private static final String placaCB = "OKZ2222";

	private static final Integer anoCB = 1;

	private static final ModeloBuilder modeloCB = ModeloBuilder.insertModelo();

	private static final Double valorDiariaCB = 2D;

	private static final SituacaoVeiculoEnum situacaoCB = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa = placaCB;

	private Integer ano = anoCB;

	private ModeloBuilder modelo = modeloCB;

	private Double valorDiaria = valorDiariaCB;

	private SituacaoVeiculoEnum situacao = situacaoCB;

	public VeiculoBuilder() {
		
	}

	public static VeiculoBuilder insertVeiculo() {
		return new VeiculoBuilder();
	}

	public String getPlaca() {
		return placa;
	}

	public VeiculoBuilder insertPlaca(String placa) {
		this.placa = placa;
		return this;

	}

	public VeiculoBuilder insertAno(Integer ano) {
		this.ano = ano;
		return this;

	}

	public VeiculoBuilder insertModelo(ModeloBuilder modelo) {
		this.modelo = modelo.mas();
		return this;

	}

	public VeiculoBuilder insertValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;

	}

	public VeiculoBuilder insertSituacaoDisponivel(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.DISPONIVEL;
		return this;

	}

	public VeiculoBuilder insertSituacaoLocado(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.LOCADO;
		return this;

	}

	public VeiculoBuilder insertSituacaoManutencao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.MANUTENCAO;
		return this;

	}

	public VeiculoBuilder insert() {
		return insertVeiculo().insertAno(ano).insertModelo(modelo).insertPlaca(placa).insertSituacaoDisponivel(situacao);
	}

	public Veiculo Builder() {
		Veiculo veiculo = new Veiculo(placa, ano, null, valorDiaria);
		veiculo.setAno(ano);
		veiculo.setPlaca(placa);
		veiculo.setSituacao(situacao);
		veiculo.setValorDiaria(valorDiaria);
		return veiculo;
	}

}
